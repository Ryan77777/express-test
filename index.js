const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require('path');

const db = require("./src/config/db");
const { uploadSingle } = require('./src/utils/multer');

const app = express();
const port = 3000;

app.use(
    cors({
        origin: "*",
    })
);

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.get("/", (req, res) => {
    res.send("Good Luck!");
});

app.get("/data", function (req, res, next) {
    const sql = "SELECT * FROM data";

    db.query(sql, function (err, result) {
        if (err) throw err;
        res.send({
            message: "Success",
            data: result
        });
    });
});

app.post("/data", uploadSingle, function (req, res, next) {
    const body = req.body;
    const photo = req.file;

    const imgUrl = `http://192.168.100.183:3000/images/${photo.filename}`;
    const location = body.location.split(", ");

    const sql = `INSERT INTO data (name, photo, latitude, longitude, created_at) VALUES ("${body.name}", "${imgUrl}", "${location[0]}", "${location[1]}", NOW())`;

    db.query(sql, function (err, result) {
        if (err) throw err;
        res.send({
            message: "Data created",
        });
    });
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});
